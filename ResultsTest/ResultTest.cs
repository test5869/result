using System;
using NUnit.Framework;
using Results;

namespace ResultsTest
{
    public class ResultTest
    {
        private const string ExceptionMessage = "something went wrong";
        private const string OkValue = "something";
        private readonly Result<string> badResult = Result<string>.Failure(new Exception(ExceptionMessage));

        private readonly Result<string> okResult = Result<string>.Success(OkValue);

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestSuccess()
        {
            var result = Result<string>.Success(OkValue);

            Assert.IsTrue(result.IsSuccess);
            Assert.IsFalse(result.IsFailure);

            Assert.AreEqual(OkValue, result.GetOrNull());
        }

        [Test]
        public void TestFailure()
        {
            var result = Result<string>.Failure(new Exception(ExceptionMessage));

            Assert.IsFalse(result.IsSuccess);
            Assert.IsTrue(result.IsFailure);

            Assert.AreEqual(null, result.GetOrNull());
        }

        [Test]
        public void TestGetOrThrow()
        {
            Assert.Throws<Exception>(() => badResult.GetOrThrow());
            Assert.AreEqual(OkValue, okResult.GetOrThrow());
        }

        [Test]
        public void TestOnSuccess()
        {
            var called = false;
            var chainCall = false;

            okResult.OnSuccess(value =>
            {
                called = true;
                Assert.AreEqual(OkValue, value);
            }).OnSuccess(_ => chainCall = true);

            Assert.True(called);
            Assert.True(chainCall);
        }

        [Test]
        public void TestOnSuccessWithFailure()
        {
            var called = false;

            badResult.OnSuccess(_ => called = true);

            Assert.False(called);
        }

        [Test]
        public void TestOnFailure()
        {
            var called = false;
            var chainCall = false;

            badResult.OnFailure(ex =>
            {
                called = true;
                Assert.AreEqual(ExceptionMessage, ex.Message);
            }).OnFailure(_ => chainCall = true);


            Assert.True(called);
            Assert.True(chainCall);
        }

        [Test]
        public void TestOnFailureWithSuccess()
        {
            var called = false;

            okResult.OnFailure(_ => called = true);

            Assert.False(called);
        }
        
        [Test]
        public void TestExceptionOrNullWithFailure()
        {

            Assert.That(okResult.ExceptionOrNull() == null);
        }

        [Test]
        public void TestExceptionOrNullWithSuccess()
        {
            Assert.That(badResult.ExceptionOrNull().Message == ExceptionMessage);

        }
    }
}