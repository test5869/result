﻿using System;

namespace Results
{
    public class Result<T>
    {
        private Exception exception;
        private T value;

        public bool IsFailure => exception != null;
        public bool IsSuccess => exception == null;

        public T GetOrThrow()
        {
            if (IsSuccess) return value;

            throw exception;
        }

        public T GetOrNull()
        {
            return value;
        }

        public Result<T> OnSuccess(Action<T> action)
        {
            if (IsSuccess) action.Invoke(value);
            return this;
        }

        public Result<T> OnFailure(Action<Exception> action)
        {
            if (IsFailure) action.Invoke(exception);
            return this;
        }

        public Exception ExceptionOrNull()
        {
            return exception;
        }

        public override string ToString()
        {
            return IsSuccess ? $"Success({value})" : exception.Message;
        }

        public static Result<T> Success(T value)
        {
            return new Result<T> {value = value};
        }

        public static Result<T> Failure(Exception exception)
        {
            return new Result<T> {exception = exception};
        }
    }
}